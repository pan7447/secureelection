App = {
  web3Provider: null,
  contracts: {},
  account: '0x0',
  votedCandidateID: 0,

  init: function() {
    return App.initWeb3();
  },

  // Checks if Metamask has provided with an instance of Web3
  // If not, default is created
  initWeb3: function() {
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
      web3 = new Web3(web3.currentProvider);
    } else {
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
      web3 = new Web3(App.web3Provider);
    }
    return App.initContract();
  },

  // INitialize the smart contract and connect the provider to communicate with it
  initContract: function() {
    $.getJSON("Election.json", function(election) {
      App.contracts.Election = TruffleContract(election);
      App.contracts.Election.setProvider(App.web3Provider);
      App.watchTimeOutEvent();
      App.watchEventDidVote();
      return App.render();
    });
  },

  // Function called when the form is submitted/ when vote is submitted by clicking on the button 'Vote'
  castVote: function() {
    var electionCandidateId = $('#CANDIDATE_CHOICE').val();
    App.contracts.Election.deployed().then(function(inst) {
      return inst.castVote(electionCandidateId, { from: App.account });
    }).then(function(result) {
      $("#CONTENT_WINDOW").hide();
      $("#LOADING_WINDOW").show();
    }).catch(function(err) {
      console.error(err);
    });
  },

  // Function that watches the event of time out
  watchTimeOutEvent: function() {
    App.contracts.Election.deployed().then(function(inst) {
      inst.timeOutEvent({}, {
        fromBlock: 'latest'
      }).watch(function(error, event) {
        var ack =  $("#ACK");
        ack.empty();
        App.render();
      });
    });
  },

  // Function that watches the event whihc observer if a voter submitted their vote
  watchEventDidVote: function() {
    App.contracts.Election.deployed().then(function(inst) {
      inst.didVoteEvent({}, {
        fromBlock: 'latest'
      }).watch(function(error, event) {
        var _id = Number(event.args._electionCandidateId);
        App.votedCandidateID = _id;
        App.render();
      });
    });
  },

  // Give the name of the month by taking index as input parameter
  getMonthName: function(m){
    var x="";
    switch(m) {
      case 0:
      x = "Jan"
      break;
      case 1:
      x = "Feb"
      break;
      case 2:
      x = "Mar"
      break;
      case 3:
      x = "Apr"
      break;
      case 4:
      x = "May"
      break;
      case 5:
      x = "Jun"
      break;
      case 6:
      x = "Jul"
      break;
      case 7:
      x = "Aug"
      break;
      case 8:
      x = "Sep"
      break;
      case 9:
      x = "Oct"
      break;
      case 10:
      x = "Nov"
      break;
      case 11:
      x = "Dec"
      break;
      default:
      console.log("invalid month");
    }
    return x;
  },

  // Function that loads the content to be displayed on the web page
  render: function() {
    var electionInst;
    var loadingWindow = $("#LOADING_WINDOW");
    var contentWindow = $("#CONTENT_WINDOW");
    var timeoutWindow = $("#TIMEOUT_WINDOW");
    var ack =  $("#ACK");
    ack.empty();
    loadingWindow.show();
    contentWindow.hide();
    timeoutWindow.hide();

    // Loading account data and displays on the web page
    web3.eth.getCoinbase(function(err, acc) {
      if (err === null) {
        App.account = acc;
        $("#ACCOUNT_ADDRESS").html("My Account: " + acc);
      }
    });


    App.contracts.Election.deployed().then(function(inst) {
      electionInst = inst;    
      return electionInst.totalCandidates();
    }).then(function(totalCandidates) {


      // To get start and end time of election and display in the web page
      electionInst.startTime().then(function(startTime){
        return startTime;
      }).then(function(startTime) {

        electionInst.endTime().then(function(endTime){

          var start = new Date(startTime*1000);
          var startMonthIndex = start.getMonth();
          var startMonth = App.getMonthName(startMonthIndex);
          var startDate = start.getDate();
          var startYear = start.getFullYear();          
          var startHours = start.getHours();
          var startMinutes = "0" + start.getMinutes();
          var startSeconds = "0" + start.getSeconds();

          var end = new Date(endTime*1000);
          var endMonthIndex = end.getMonth();
          var endMonth = App.getMonthName(endMonthIndex);
          var endDate = end.getDate();
          var endYear = end.getFullYear();          
          var endHours = end.getHours();
          var endMinutes = "0" + end.getMinutes();
          var endSeconds = "0" + end.getSeconds();

          var election_begin = 'Eection start timestamp: ' + startMonth + ' ' + startDate + ', '+ startYear + ' at '+ startHours + ':' + startMinutes.substr(-2) + ':' + startSeconds.substr(-2);
          var  election_end = 'Election end timestamp: ' + endMonth + ' ' + endDate + ', '+ endYear + ' at '+ endHours + ':' + endMinutes.substr(-2) + ':' + endSeconds.substr(-2);
          var duration = "Duration: " + (Number(endTime)-Number(startTime))/60 + " minutes";
          $("#START").text(election_begin);
          $("#END").text(election_end);
          $("#TIME").text(duration);
        });
      });


      var table_div = $("#TABLE");
      table_div.empty();
      var candidatesChoice = $('#CANDIDATE_CHOICE');
      candidatesChoice.empty();

      // To get election candidate details and total votes obtained till now and displa on the web page
      electionInst.electionCandidates(1).then(function(ElectionCandidate1){
        return ElectionCandidate1;
      }).then(function(ElectionCandidate1) {

        electionInst.electionCandidates(2).then(function(ElectionCandidate2){

          var electionCandidateId1 = ElectionCandidate1[0];
          var electionCandidateName1 = ElectionCandidate1[1];
          var totalVotes1 = ElectionCandidate1[2];

          var electionCandidateId2 = ElectionCandidate2[0];
          var electionCandidateName2 = ElectionCandidate2[1];
          var totalVotes2 = ElectionCandidate2[2];
          var table_div = $("#TABLE");
          table_div.empty();
          var table_content ='<table class="table" ><thead id="RESULTS_HEADINGS"><tr><th scope="col" class="table-content">Candidate ID</th><th scope="col" class="table-content">Candidate Name</th></tr></thead><tbody id="RESULTS"><tr><td class="table-content">' + electionCandidateId1 + '</td><td class="table-content">' + electionCandidateName1 + '</td></tr><tr><td class="table-content">' + electionCandidateId2 + '</td><td class="table-content">' + electionCandidateName2 + '</td></tr></tbody></table>';
          table_div.append(table_content);
        
          var selectedCandidate1 = "<option value='" + electionCandidateId1 + "' >" + electionCandidateName1 + "</ option>";
          candidatesChoice.append(selectedCandidate1);

          var selectedCandidate2 = "<option value='" + electionCandidateId2 + "' >" + electionCandidateName2 + "</ option>";
          candidatesChoice.append(selectedCandidate2);
        });
      });
      console.log("render");
      return electionInst.timeoutLog(App.account);
    }).then(function(didTimeOut) {

      // Runs when election time is over and displays the results
      if(didTimeOut) {
        $('form').hide();
        var table_div = $("#TABLE");
        table_div.empty();
              
       var candidatesChoice = $('#CANDIDATE_CHOICE');
       candidatesChoice.empty();

      // To get election candidate details and total votes obtained till now and displa on the web page
      electionInst.electionCandidates(1).then(function(ElectionCandidate1){
        return ElectionCandidate1;
      }).then(function(ElectionCandidate1) {

        electionInst.electionCandidates(2).then(function(ElectionCandidate2){

          var electionCandidateId1 = ElectionCandidate1[0];
          var electionCandidateName1 = ElectionCandidate1[1];
          var totalVotes1 = ElectionCandidate1[2];

          var electionCandidateId2 = ElectionCandidate2[0];
          var electionCandidateName2 = ElectionCandidate2[1];
          var totalVotes2 = ElectionCandidate2[2];
          var table_div = $("#TABLE");
          table_div.empty();
          
          var table_content ='<table class="table" ><thead id="RESULTS_HEADINGS"><tr><th scope="col" class="table-content">Candidate ID</th><th scope="col" class="table-content">Candidate Name</th><th scope="col" class="table-content">Total Votes</th></tr></thead><tbody id="RESULTS"><tr><td class="table-content">' + electionCandidateId1 + '</td><td class="table-content">' + electionCandidateName1 + '</td><td class="table-content">' + totalVotes1 + '</td></tr><tr><td class="table-content">' + electionCandidateId2 + '</td><td class="table-content">' + electionCandidateName2 + '</td><td class="table-content">' + totalVotes2 + '</td></tr></tbody></table>';
          table_div.append(table_content);

          var _selectedCandidate1 = "<option value='" + electionCandidateId1 + "' >" + electionCandidateName1 + "</ option>";
          candidatesChoice.append(_selectedCandidate1);
           
          var _selectedCandidate2 = "<option value='" + electionCandidateId2 + "' >" + electionCandidateName2 + "</ option>";
          candidatesChoice.append(_selectedCandidate2);

        });
      });

        timeoutWindow.show();


        var lead = $("#LEAD");
        lead.empty();

        electionInst.electionCandidates(1).then(function(ElectionCandidate1){
          return ElectionCandidate1;
        }).then(function(ElectionCandidate1) {

          electionInst.electionCandidates(2).then(function(ElectionCandidate2){

            if(Number(ElectionCandidate1[2]) > Number(ElectionCandidate2[2])){
              var trumpImg = '<img src="images/trump.png" alt="Trump is leading ... height="300px" width="300px">'
              lead.append(trumpImg);
            }
            else if(Number(ElectionCandidate1[2]) < Number(ElectionCandidate2[2])){
              var clintonImg = '<img src="images/clinton.png" alt="Clinton is leading ... height="350px" width="350px" >'
              lead.append(clintonImg);
            }
            else{
              var tieImg = '<img src="images/no.png" alt="Election tie ... "  height="300px" width="300px">'
              lead.append(tieImg);
            }

          });
        });
      }
      return electionInst.votingCandidates(App.account);
    }).then(function(didVote) {

      // Runs when a person cast theri vote and hides the voting form
      if(didVote) {
        $('form').hide();
        var ack =  $("#ACK");
        ack.empty();
        
        if(App.votedCandidateID == 1){
          var txt = '<h4 class="text-center" id="ACK">You voted for Trump</h4>'
          ack.append(txt);
        }
        else{
          var txt = '<h4 class="text-center" id="ACK">You voted for Clinton</h4>'
          ack.append(txt);
        }
      }
      loadingWindow.hide();
      contentWindow.show();
    }).catch(function(error) {
      console.warn(error);
    });
  }
};

$(function() {
  $(window).load(function() {
    App.init();
  });
});