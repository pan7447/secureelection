var Election = artifacts.require("./Election.sol");
var sleep = require('system-sleep');

contract("Election", function(accounts) {
	var electionInst;

	it("Created 2 candidates", function() {
		return Election.deployed().then(function(instance) {
			return instance.totalCandidates();
		}).then(function(count) {
			assert.equal(count, 2);
		});
	});

	it("Verifying Candidate IDs", function() {
		return Election.deployed().then(function(inst) {
			electionInst = inst;
			return electionInst.electionCandidates(1);
		}).then(function(electionCandidates) {
			assert.equal(electionCandidates[0], 1, "check electionCandidateId for Trump"); 
			return electionInst.electionCandidates(2);
		}).then(function(electionCandidates) {
			assert.equal(electionCandidates[0], 2, "check electionCandidateId for Clinton");
		});
	});

	it("Verifying Candidate Names", function() {
		return Election.deployed().then(function(inst) {
			electionInst = inst;
			return electionInst.electionCandidates(1);
		}).then(function(electionCandidates) {
			assert.equal(electionCandidates[1], "Trump", "check electionCandidateName");
			return electionInst.electionCandidates(2);
		}).then(function(electionCandidates) {
			assert.equal(electionCandidates[1], "Clinton", "check electionCandidateName");
		});
	});

	it("Verifying Vote counts for Candidates", function() {
		return Election.deployed().then(function(inst) {
			electionInst = inst;
			return electionInst.electionCandidates(1);
		}).then(function(electionCandidates) {
			assert.equal(electionCandidates[2], 0, "check totalVotes for Trump");
			return electionInst.electionCandidates(2);
		}).then(function(electionCandidates) {
			assert.equal(electionCandidates[2], 0, "check totalVotes for Clinton");
		});
	});



	it("Checking if a voter can cast a vote for Trump using first account", function() {
		return Election.deployed().then(function(inst) {
			electionInst = inst;
			electionCandidateId = 1;
			return electionInst.castVote(electionCandidateId, { from: accounts[0] });
		}).then(function(receipt) {
			return electionInst.votingCandidates(accounts[0]);
		}).then(function(voted) {
			assert(voted, "the candidate who voted is saved as voted");
			return electionInst.electionCandidates(electionCandidateId);
		}).then(function(electionCandidate) {
			var totalVotes = electionCandidate[2];
			assert.equal(totalVotes, 1, "Increment the totalVotes");
		})
	});

	it("Checking if a voter can cast a vote for Clinton using sixth account", function() {
		return Election.deployed().then(function(inst) {
			electionInst = inst;
			electionCandidateId = 2;
			return electionInst.castVote(electionCandidateId, { from: accounts[6] });
		}).then(function(receipt) {
			return electionInst.votingCandidates(accounts[6]);
		}).then(function(voted) {
			assert(voted, "the candidate who voted is saved as voted");
			return electionInst.electionCandidates(electionCandidateId);
		}).then(function(electionCandidate) {
			var totalVotes = electionCandidate[2];
			assert.equal(totalVotes, 1, "Increment the totalVotes");
		})
	});

	it("If election candidate id is invalid when voting, throw exception", function() {
		return Election.deployed().then(function(inst) {
			electionInst = inst;
			return electionInst.castVote(25, { from: accounts[5] })
		}).then(assert.fail).catch(function(error) {
			assert(error.message.indexOf('revert') >= 0, "Revert should be there in the error msg");
		});
	});

	it("Verifying Vote counts for Candidates to check if it is not changed", function() {
		return Election.deployed().then(function(inst) {
			electionInst = inst;
			return electionInst.electionCandidates(1);
		}).then(function(electionCandidates) {
			assert.equal(electionCandidates[2], 1, "totalVotes didn't change for Trump");
			return electionInst.electionCandidates(2);
		}).then(function(electionCandidates) {
			assert.equal(electionCandidates[2], 1, "totalVotes didn't change for Clinton");
		});
	});

	it("Check if an exception is thrown on double voting by a voter using account 7", function() {
		return Election.deployed().then(function(inst) {
			electionInst = inst;
			electionCandidateId = 2;
			electionInst.castVote(electionCandidateId, { from: accounts[7] });
			return electionInst.electionCandidates(electionCandidateId);
		}).then(function(election_candidate) {
			var totalVotes = election_candidate[2];
			assert.equal(totalVotes, 2, "Casted vote once");
			return electionInst.castVote(electionCandidateId, { from: accounts[7] });
		}).then(assert.fail).catch(function(error) {
			assert(error.message.indexOf('revert') >= 0, error.message + "revert should be there in the error message");
		});

	});

	it("Verifying Vote counts for Candidates to check if it is not changed", function() {
		return Election.deployed().then(function(inst) {
			electionInst = inst;
			return electionInst.electionCandidates(1);
		}).then(function(electionCandidates) {
			assert.equal(electionCandidates[2], 1, "totalVotes didn't change for Trump");
			return electionInst.electionCandidates(2);
		}).then(function(electionCandidates) {
			assert.equal(electionCandidates[2], 2, "totalVotes didn't change for Clinton");
		});
	});

	it("Checking if page get reloaded after voting", function() {
		return Election.deployed().then(function(inst) {
			electionInst = inst;
			electionCandidateId = 1;
			return electionInst.castVote(electionCandidateId, { from: accounts[8] });
		}).then(function(receipt) {
			assert.equal(receipt.logs.length, 1, "casting vote triggered event");
			assert.equal(receipt.logs[0].event, "didVoteEvent", "didVoteEvent event has been triggered");
			assert.equal(receipt.logs[0].args._electionCandidateId.toNumber(), electionCandidateId, "the candidate id is correct");
		});
	});


	it("Checking the timer functionality - vote from account 9", function() {
		return Election.deployed().then(function(inst) {
			electionInst = inst;
			electionCandidate = 2;
			sleep(120000);
			return electionInst.castVote(electionCandidate, { from: accounts[9] })
		}).then(assert.fail).catch(function(error) {
			assert(error.message.indexOf('revert') >= 0, error.message + "Revert should be there in the error msg");
		});
	})


	it("Verifying Vote counts for Candidates to check if it is not changed", function() {
		return Election.deployed().then(function(inst) {
			electionInst = inst;
			return electionInst.electionCandidates(1);
		}).then(function(electionCandidates) {
			assert.equal(electionCandidates[2], 2, "totalVotes didn't change for Trump");
			return electionInst.electionCandidates(2);
		}).then(function(electionCandidates) {
			assert.equal(electionCandidates[2], 2, "totalVotes didn't change for Clinton");
		});
	});


});