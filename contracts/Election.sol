pragma solidity ^0.4.2;
contract Election {

	// Structure of election candidate
	struct ElectionCandidate {
		uint electionCandidateId;
		string electionCandidateName;
		uint totalVotes;
	}

	// Map for tracking election candidates and their count of votes obtained
	mapping(uint => ElectionCandidate) public electionCandidates;
	// Map for tracking users who voted in elections
	mapping(address => bool) public votingCandidates;
	// Map to track users who couldn't vote before deadline
	mapping(address => bool) public timeoutLog;
	// Address of the first block
	address public firstBlockAddress;
	// Election start time (UTC)
	uint public startTime;
	// Election end time (UTC)
	uint public endTime;
	// To track count of election candidates
	uint public totalCandidates;

	// constructor where 2 new candidates, Trump and Clinton are added,
	// firstBlockAddress is obtained who writes for the first time to block chain,
	// startTime is initialized to current UTC time,
	// endTime is initialized as per choice after deciding on duration of election (currently 10 minutes)
	function Election () public {
		addElectionCandidate("Trump");
		addElectionCandidate("Clinton");
		firstBlockAddress = msg.sender;
		startTime=now;
		//change 10 to 2 for test script
		endTime = startTime+ 10 minutes;
	}

	// Function that initializes each election candidate as when called by the construcor
	// totalVotes is initialized to  at the start
	function addElectionCandidate (string _electionCandidateName) private {
		totalCandidates ++;
		electionCandidates[totalCandidates] = ElectionCandidate(totalCandidates, _electionCandidateName, 0);
	}

	// Event used to track when a user finishes voting an election candidate
	event didVoteEvent (
		uint _electionCandidateId
	);

	// Event used to track if the deadline to vote is crossed
	event timeOutEvent (
		uint _electionCandidateId
	);

	// Function where voting actually happens after certain conditions are checked
	function castVote (uint _electionCandidateId) public {
		//checks if the election duration is over, if yes, then event timeOutEvent is notified
		if(endTime<now){ 
			timeoutLog[msg.sender] = true;
			timeOutEvent(_electionCandidateId); 
    		// add this line only when test script is to be run 	
    		//require(endTime > now);    	
    	}
    	// Runs if there is still time for deadline
    	else{
    		// Checks if the user hasn't already voted
    		require(!votingCandidates[msg.sender]);
    		// Checks if the user is voting for the right candidate
    		// Only required while testing
    		// require(_electionCandidateId > 0);
    		// require(_electionCandidateId <= totalCandidates);

    		// Once user votes, the voteCount of voted candidates are incremented
    		electionCandidates[_electionCandidateId].totalVotes ++;
    		// Voter is stored in the map once voted 
    		// Tracking done to check if the user doesn't vote again
    		votingCandidates[msg.sender] = true;   
    		// Once voted, didVoteEvent is notified 		
    		didVoteEvent(_electionCandidateId);
    	}
    }

    

}
